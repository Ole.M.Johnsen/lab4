package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private CellState[][] grid;
    private int rows;
    private int columns;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        grid = new CellState[this.rows][this.columns];
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < columns; col++) {
                grid[row][col] = initialState;
            }
        }
    }

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid copy = new CellGrid(numRows(), numColumns(), CellState.DEAD);
        for (int row = 0; row < numRows(); row++) {
            for (int col = 0; col < numColumns(); col++) {
                copy.set(row, col, get(row, col));
            }
        }
        
        return copy;
    }
    
}
